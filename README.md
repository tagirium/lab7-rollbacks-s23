# Lab7 - Rollbacks and reliability

## Part I

All trnsactions should go on 1 connection and 1 cursor. If transaction failed at any step - it's rollback, otherwise - commit

## Part II

Code for creating Inventory table:
```postgresql
CREATE TABLE Inventory (
    username TEXT REFERENCES Player(username) NOT NULL,
    product  TEXT REFERENCES Shop(product) NOT NULL,
    amount   INT CHECK (amount >= 0),
    PRIMARY KEY(username, product)
)
```
Two additional queries were added, one for getting inventory amount:
```postgresql
SELECT sum(amount)
FROM Inventory 
WHERE username = %(username)s;
```
And one for updating the inventory:
```postgresql
INSERT INTO Inventory (username, product, amount) 
VALUES (%(username)s, %(product)s, %(amount)s) 
ON CONFLICT (username, product) 
DO UPDATE SET amount = Inventory.amount + %(amount)s"
```
