import psycopg2


price_request = "SELECT price FROM Shop WHERE product = %(product)s"
buy_decrease_balance = f"UPDATE Player SET balance = balance - ({price_request}) * %(amount)s WHERE username = %(username)s"
buy_decrease_stock = "UPDATE Shop SET in_stock = in_stock - %(amount)s WHERE product = %(product)s"
get_inventory_amount = "SELECT sum(amount) FROM Inventory WHERE username = %(username)s"
upd_inventory = "INSERT INTO Inventory (username, product, amount) VALUES (%(username)s, %(product)s, %(amount)s) ON CONFLICT (username, product) DO UPDATE SET amount = Inventory.amount + %(amount)s"

inv_lim = 100

def get_connection():
    return psycopg2.connect(
        dbname="psql",
        user="admin",
        password="admin",
        host="127.0.0.1",
        port=5432)


def buy_product(username, product, amount):
    obj = {"product": product, "username": username, "amount": amount}
    with get_connection() as conn:
        with conn.cursor() as cur:
            try:
                cur.execute(buy_decrease_balance, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong username")
            cur.execute(buy_decrease_stock, obj)

                if cur.rowcount != 1:
                    raise Exception("Wrong product or out of stock")

                cur.execute(get_inventory_amount, obj)
                inventory_amount = cur.fetchone()
                if inventory_amount is None:
                    cur.execute(update_inventory, obj)
                    if cur.rowcount != 1:
                        raise Exception("Inventory update failed")
                    cur.commit()
                    return
                if inventory_amount[0] is None:
                    inventory_amount = 0
                else:
                    inventory_amount = inventory_amount[0]
                if inv_lim - inventory_amount < amount:
                    raise Exception("Inventory amount is exceeded")
                if cur.rowcount != 1:
                    raise Exception("Inventory update failed")
                cur.commit()
            except Exception as e:
                cur.rollback()
                raise e


buy_product('Alice', 'marshmello', 1)
